#ifndef couplingAnalysis_Categorization_H
#define couplingAnalysis_Categorization_H

#include "HGamAnalysisFramework/HgammaAnalysis.h"
#include <map>

class Categorization : public HgammaAnalysis
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:

  bool m_useSystematics;
  bool m_reweightHiggsPt;
  bool m_isGGH;

  bool m_splitTTH;

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
private:
  // Tree *myTree; //!
  // TH1 *myHist; //!
  //std::map<int,int> HTXSall0; //!
  std::map<int,int> HTXSmybin; //!

  TString binNames[17] = {"Unknown","ggH_Fwd","ggH","VBF_Fwd","VBF",
    "WH_Fwd","WH","ZH_Fwd","ZH","VHQQ_Fwd","VHQQ","ttH_Fwd","ttH",
    "bbH_Fwd","bbH","tH_Fwd","tH"}; //!
  
public:
  // this is a standard constructor
  Categorization() { }
  Categorization(const char *name);
  virtual ~Categorization();

  // these are the functions inherited from HgammaAnalysis
  virtual EL::StatusCode createOutput();
  virtual EL::StatusCode execute();
  double ggF_01jet_hpT_weight(double H_pT);


  // this is needed to distribute the algorithm to the workers
  ClassDef(Categorization, 1);
};

#endif // couplingAnalysis_Categorization_H
