#include "couplingAnalysis/Categorization.h"
#include "HGamAnalysisFramework/HgammaIncludes.h"
#include "HGamAnalysisFramework/HGamVariables.h"
#include <map>

// this is needed to distribute the algorithm to the workers
ClassImp(Categorization)



Categorization::Categorization(const char *name)
: HgammaAnalysis(name)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
}



Categorization::~Categorization()
{
  // Here you delete any memory you allocated during your analysis.
}


double Categorization::ggF_01jet_hpT_weight(double H_pT) {
  if (H_pT<20)  return 1.11;
  if (H_pT<45)  return 1.11 - (H_pT-20)/25*0.2;  // -> 0.91
  if (H_pT<135) return 0.91 - (H_pT-45)/90*0.36; // -> 0.55
  return 0.55;
}


EL::StatusCode Categorization::createOutput()
{
  std::cout << "Looping over systematics..." << std::endl;
  m_useSystematics = config()->getBool("HGamCoupling.UseSystematics", false);
  m_reweightHiggsPt = config()->getBool("HGamCoupling.ReweightHiggsPt", false);
  
  // should we split the ttH categories? 
  m_splitTTH = config()->getBool("HGamCoupling.SplitTTH", false);

  // Only apply the reweighting to ggH MC
  if (isMC()) m_reweightHiggsPt &= getMCSampleName(eventInfo()->mcChannelNumber()).Contains("ggH125");
  else m_reweightHiggsPt = false;

  if (m_reweightHiggsPt) std::cout << "*** !!! REWEIGHTING HIGGS PT !!! ***" << std::endl;
  else std::cout << "*** NO HIGGS PT REWEIGHTING USED ***" << std::endl;
  
  TString suffix = ""; 
  for (auto sys: getSystematics()) {

    if (sys.name() != "") {
      if (not m_useSystematics) break;
      TString sysName = sys.name();
      if (sysName.Contains("Trig")) continue;
      if (sysName.Contains("_CorrUncertainty")) continue;
      suffix = "_" + sys.name();
      suffix.ReplaceAll(" ","_");
    }

    std::cout << "   SYSTEMATIC :: " << sys.name() << std::endl;
    histoStore()->createTH1F("h_category"+suffix, 15, -0.5, 16.5);
    histoStore()->createTH1F("h_inclusive"+suffix, 1, -5, 5);
    histoStore()->createTH2F("h2_truthBin_Cat"+suffix,       17, -0.5, 16.5, 15, -0.5, 16.5);
    histoStore()->createTH2F("h2_truthBin_Cat_wCat"+suffix,  17, -0.5, 16.5, 15, -0.5, 16.5);
    histoStore()->createTH2F("h2_truthBin_Cat_wInit"+suffix, 17, -0.5, 16.5, 15, -0.5, 16.5);
    histoStore()->createTH2F("h2_truthBin_Cat_RAW"+suffix,   17, -0.5, 16.5, 15, -0.5, 16.5);
    histoStore()->createTH2F("h2_noDalitz_truthBin_Cat"+suffix,       17, -0.5, 16.5, 15, -0.5, 16.5);
    histoStore()->createTH2F("h2_noDalitz_truthBin_Cat_wCat"+suffix,  17, -0.5, 16.5, 15, -0.5, 16.5);
    histoStore()->createTH2F("h2_noDalitz_truthBin_Cat_wInit"+suffix, 17, -0.5, 16.5, 15, -0.5, 16.5);
    histoStore()->createTH2F("h2_noDalitz_truthBin_Cat_RAW"+suffix,   17, -0.5, 16.5, 15, -0.5, 16.5);

  }
  
  // ------------------------------------
  //   Define new binning
  // ------------------------------------
  HTXSmybin[0]  =  0;    // Unknown or data
  HTXSmybin[10] =  1;    // ggH_Fwd
  HTXSmybin[11] =  2;    // ggH
  HTXSmybin[20] =  3;    // VBF_Fwd
  HTXSmybin[21] =  4;    // VBF
  HTXSmybin[30] =  5;    // WH_Fwd
  HTXSmybin[31] =  6;    // WH
  HTXSmybin[40] =  7;    // ZH_Fwd
  HTXSmybin[41] =  8;    // ZH
  HTXSmybin[90] =  9;    // VH(QQ)_Fwd
  HTXSmybin[91] = 10;    // VH(QQ)
  HTXSmybin[60] = 11;    // ttH_Fwd
  HTXSmybin[61] = 12;    // ttH
  HTXSmybin[70] = 13;    // bbH_Fwd
  HTXSmybin[71] = 14;    // bbH
  HTXSmybin[80] = 15;    // tH_Fwd
  HTXSmybin[81] = 16;    // tH

  std::cout << std::endl << std::endl;
  
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode Categorization::execute()
{
  HgammaAnalysis::execute();

  // Get the Higgs pT weight, based on Njets pT > 25 GeV and pTH
  //  --> higgs pT weighting to only be applied on ggH samples
  double w_pT = 1.0;
  if ( isMC() && m_reweightHiggsPt ) { 
    int Nj = var::N_j.truth();
    double pTH = var::pT_h1.truth()*0.001;
    if (Nj<2) w_pT = ggF_01jet_hpT_weight(pTH);
  } 
  
  // Get Higgs Truth Bin and Cross-Sections
  int truthBin = (isData()) ? -1 : HTXSmybin[var::HTXS_phase0.truth()];
  double norm = (isData() || m_useSystematics) ? 1.0 : lumiXsecWeight();

  // Loop over systematic variations
  TString suffix = "";
  for (auto sys: getSystematics()) {
    if (sys.name() != "") {
      if (not m_useSystematics) break;
      if (isData()) break;
      TString sysName = sys.name();
      if (sysName.Contains("Trig")) continue;
      if (sysName.Contains("_CorrUncertainty")) continue;
      suffix = "_" + sys.name();
      suffix.ReplaceAll(" ","_");
      applySystematicVariation(sys);
    }

    double w     = (isData()) ? 1.0 : w_pT * norm * var::weightCatCoup_dev();
    double wCat  = (isData()) ? 1.0 : w_pT * var::weightCatCoup_dev();
    double wInit = (isData()) ? 1.0 : w_pT * weightInitial();;

    histoStore()->fillTH2F("h2_truthBin_Cat"+suffix,       truthBin, 0, w);
    histoStore()->fillTH2F("h2_truthBin_Cat_wCat"+suffix,  truthBin, 0, wCat);
    histoStore()->fillTH2F("h2_truthBin_Cat_wInit"+suffix, truthBin, 0, wInit);
    histoStore()->fillTH2F("h2_truthBin_Cat_RAW"+suffix,   truthBin, 0, 1.0);

    if (not var::isDalitzEvent()) { 
      histoStore()->fillTH2F("h2_noDalitz_truthBin_Cat"+suffix,       truthBin, 0, w);
      histoStore()->fillTH2F("h2_noDalitz_truthBin_Cat_wCat"+suffix,  truthBin, 0, wCat);
      histoStore()->fillTH2F("h2_noDalitz_truthBin_Cat_wInit"+suffix, truthBin, 0, wInit);
      histoStore()->fillTH2F("h2_noDalitz_truthBin_Cat_RAW"+suffix,   truthBin, 0, 1.0);
    }

    if (not var::isPassed()) continue;

    int catCoup = var::catCoup_dev();
    if (m_splitTTH) {
      if (catCoup == 12 && eventHandler()->getVar<int>("N_j_btag30") > 1 ) catCoup = 13;
      if (catCoup == 13 && eventHandler()->getVar<int>("N_j_btag")  <= 1 ) catCoup = 14;
      if (catCoup == 13 && eventHandler()->getVar<int>("N_j_btag")   > 1 ) catCoup = 15;
    }
    
    if (isData()) {
      // Tell me a bit about the data 
      //std::cout << "data cat " << var::catCoup_dev() << "  ::  " << var::m_yy()*1e-3 
      std::cout << "data cat " << catCoup << "  ::  " << var::m_yy()*1e-3 
                << "  ::   runN = " << eventInfo()->runNumber() 
                << "  evtN = " << eventInfo()->eventNumber() 
                << "  N_mu = " << var::N_mu() 
                << "  N_el = " << var::N_e() 
                << "  Ntag = " << eventHandler()->getVar<int>("N_j_btag")
                << "  MET = " << var::met_TST()*0.001
                << "  m_mumu = " << var::m_mumu()*0.001
                << std::endl;
    }
    
    histoStore()->fillTH1F("h_inclusive"+suffix, 0, w);
    histoStore()->fillTH1F("h_category"+suffix, catCoup, w); 

    histoStore()->fillTH2F("h2_truthBin_Cat"+suffix,       truthBin, catCoup, w);
    histoStore()->fillTH2F("h2_truthBin_Cat_wCat"+suffix,  truthBin, catCoup, wCat);
    histoStore()->fillTH2F("h2_truthBin_Cat_wInit"+suffix, truthBin, catCoup, wInit);
    histoStore()->fillTH2F("h2_truthBin_Cat_RAW"+suffix,   truthBin, catCoup, 1.0);
  
    if (not var::isDalitzEvent()) { 
      histoStore()->fillTH2F("h2_noDalitz_truthBin_Cat"+suffix,       truthBin, catCoup, w);
      histoStore()->fillTH2F("h2_noDalitz_truthBin_Cat_wCat"+suffix,  truthBin, catCoup, wCat);
      histoStore()->fillTH2F("h2_noDalitz_truthBin_Cat_wInit"+suffix, truthBin, catCoup, wInit);
      histoStore()->fillTH2F("h2_noDalitz_truthBin_Cat_RAW"+suffix,   truthBin, catCoup, 1.0);
    }

  }


  return EL::StatusCode::SUCCESS;
}
